#!/usr/bin/env python3

from setuptools import setup

setup(
    name='trydiffoscope',
    version='67.0.8',
    author="Chris Lamb",
    author_email="lamby@debian.org",
    scripts=('trydiffoscope',),
)
